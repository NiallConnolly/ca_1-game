#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;


//functions
/////////////////
//validator function
string process_Validator(vector <string> validEntries);
//game function
void game();



int main()
{
	game();
        
	system("pause");
	return 0;
}



// validator reads in vector of strings which are valid inputs associated with location in game
string process_Validator(vector <string> validEntries)
{
	string valid = "false";
	string userInput = "";

	while(valid != "true")
	{
		cout << "> ";
		cin >> userInput;

		for(int i = 0; i != validEntries.size(); ++i)
		{
			string compare = (string)validEntries[i];
			if(userInput == validEntries[i])
			{
				valid = "true";
				return userInput;
			}

		}
		if(valid != "true")
		{
			return "not valid";
		}
		 
	}

}

void game()
{
	// all locations in game

	string ward = "ward";
	string balcony = "balcony";
	string storage = "storage";
	string corridor = "corridor";
	string reception = "reception";
	string finish = "stairs";

	// location descriptions

	string wardDescription = "The ward is full of other patients beds and belongings. There is a note on the table beside you. There is a balcony to your left. There is also a door to your right.";
	string balconyDescription = "The balcony is littered with cigeratte butts and plastic cups. There is also rope made of bed sheets hanging down to a room below.";
	string storageDescription = "The storage room is in a mess with scattered sheets and empty bottles of cleaning fluids and products on floor. There is a door which leads to corridor and beside it is a bagpack.";
	string corridorDescription = "The corridor is filled with wheelchairs and strechers. There seems to be a lamp on up ahead at the floor receiption";
	string receptionDescription = "The reception floor is covered in papers and blood. Every door seems to be barcaded except for stairwell. The receiption desk is cluttered with pens and paper there is also a note attached to drawer.";
	string finishDescription = "This will take me down to main floor and exit. It is also dark and eerie looking. Hopeful I can find someone and find out what is going on";

	 //setting starting position

	string location = ward;
	string locationFormat = "Location - ";
	string locationDescription = wardDescription;
	string name;

	//passing in valid strings for each location
	vector <string> wardValid;
	wardValid.push_back ("note"); 
	wardValid.push_back("balcony");
	wardValid.push_back("door");
	vector <string> balconyValid;
	balconyValid.push_back("ward");
	balconyValid.push_back("climb");
	vector <string> storageValid;
	storageValid.push_back("bagpack");
	storageValid.push_back("note");
	storageValid.push_back("corridor");
	storageValid.push_back("door");
	vector <string> corridorValid;
	corridorValid.push_back("storage");
	corridorValid.push_back("receiption");
	vector <string> receptionValid;
	receptionValid.push_back("corridor");
	receptionValid.push_back("note");
	receptionValid.push_back("drawer");
	receptionValid.push_back("door");
	vector <string> currentValid = wardValid;

	cout << "Please enter your name\n> ";
	cin >> name;


	
	string wardNote = "Dear " + name +", \nIf you are reading this you have finally awoken from your coma.\nThe doctors told us that you had a 50/50 chance of waking up after that severe car accident you were in. \n We had to leave due to the riots that have suddenly happened across the country people \n call it a revolution others think it something affecting the people of the country's judgement\n You'll find a key beside the note it opens the storage room down stairs.\n I left some supplies there for you and clothes. \nJack";
	string storageNote = "Dear " + name +", \nI left some food and water in a bagpack here for you. There is a key to open up door to main corridor to receiption. I also left a crowbar as weapon to help you for any situation dont trust anyone!";
	string receptionNote = "Dear staff \n Due to the riots in the city all stairwell doors have been locked. \n Only authorised people have keys to move between floors. We just cant \nput the safety of our patients in jeopardy.";
	string door = "The door is locked";
	bool storageKey = false;
	bool receptionKey = false;
	string storageKeyAcquired = "You have acquired key to storage";
	string receptionKeyAcquired = "You have acquired key to staircase";

	string playerInput = "";


	

	while(location != finish)
	{
		if(location == ward)
		{
			cout << locationFormat << location << endl;
			cout << locationDescription << endl;
			currentValid = wardValid;
			playerInput = process_Validator(wardValid);
		}
		else if(location == balcony)
		{
			cout << locationFormat << location << endl;
			cout << locationDescription << endl;
			currentValid = balconyValid;
			playerInput = process_Validator(balconyValid);
		}
		else if(location == storage)
		{
			cout << locationFormat << location << endl;
			cout << locationDescription << endl;
			currentValid = storageValid;
			playerInput = process_Validator(storageValid);
		}
		else if(location == corridor)
		{
			cout << locationFormat << location << endl;
			cout << locationDescription << endl;
			currentValid = corridorValid;
			playerInput = process_Validator(corridorValid);
		}
		else
		{
			cout << locationFormat << location << endl;
			cout << locationDescription << endl;
			playerInput = process_Validator(receptionValid);
		}


		if(playerInput == "note" && location == ward)
		{
			cout << wardNote << endl;
		}

		else if(playerInput == "door" && location == ward )
		{
			cout << door << endl;
		}

		else if(playerInput == ward && location == balcony)
		{
			location = ward;
			locationDescription = wardDescription;
		}

		else if(playerInput == balcony && location == ward)
		{
			location = balcony;
			locationDescription = balconyDescription;
		}

		else if(playerInput == "climb" && location == balcony)
		{
			location = storage;
			locationDescription = storageDescription;
		}

		else if(playerInput == "bagpack" && location == storage)
		{    
			cout << storageNote << endl;       
			cout << storageKeyAcquired << endl;
			storageKey = true;
		}

		else if(playerInput == "door" && location == storage && storageKey == true)
		{
			location = corridor;
			locationDescription = corridorDescription;
		}

		else if(playerInput == "door" && location == storage && storageKey != true)
		{
			cout << door << endl;
		}

		else if(playerInput == "storage" && location == corridor && storageKey == true)
		{
			location = storage;
			locationDescription = storageDescription;
		}

		else if(playerInput == reception && location == corridor)
		{
			location = reception;
			locationDescription = receptionDescription;
		}

		else if(playerInput == "corridor" && location == reception)
		{
			location = corridor;
			locationDescription = corridorDescription;
		}

		else if(playerInput == "note" && location == reception)
		{
			cout << receptionNote << endl;    
		}

		else if(playerInput == "drawer" && location == reception)
		{
			cout << receptionKeyAcquired << endl;
			receptionKey = true;

		}

		else if(playerInput == "door" && location == reception && receptionKey != true)
		{
			cout << door << endl;
		}

		else if(playerInput == "door" && location == reception && receptionKey == true)
		{
			location = finish;
			locationDescription = finishDescription;
			cout << locationFormat << location << endl;
			cout << locationDescription << endl;    

		}

		else
		{
			cout << "I can't do that" << endl;
		}

		cout << "\n" << endl;

                       
	}

}